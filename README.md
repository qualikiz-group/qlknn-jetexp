These are QuaLiKiz neural networks based on experimental JET profiles.
These networks are currently in early stages of testing, and thus
these files should not be used, copied or distributed!
For more information contact Karel van de Plassche on
k.l.vandeplassche@differ.nl or karelvandeplassche@gmail.com, and/or the
author of these networks Aaron ho at a.ho@differ.nl
